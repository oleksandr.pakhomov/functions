const getSum = (str1, str2) => {
  const isValid = validateString(str1, str2);
  if (isValid) {
    return String(Number(str1) + Number(str2));
  } return false;
}

function validateString(...strings) {
  let isValid = true;
  for (let i = 0; i < strings.length && isValid; i++) {
    if (Array.isArray(strings[i])
      || typeof strings[i] !== 'string'
      || !isFinite(strings[i])) {
      isValid = false;
    }
  }
  return isValid;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  if (!Array.isArray(listOfPosts) && typeof (authorName) !== 'string') {
    return false;
  }

  let postsCount = 0;
  let commentsCount = 0;

  listOfPosts.forEach(post => {
    if (post.author === authorName) {
      postsCount++;
    }
    if (post.comments) {
      post.comments.forEach(comment => {
        if (comment.author === authorName) {
          commentsCount++;
        }
      })
    }
  })

  return `Post:${postsCount},comments:${commentsCount}`;
};

const tickets = (people) => {
  const cashRegister = {
    25: 0,
    50: 0,
    100: 0,
  }

  function giveChange(money, cashRegister) {
    if (Number(money) === 25) {
      cashRegister[25] = cashRegister[25] + 1;
      return true;
    } else if (Number(money) === 50) {
      cashRegister[50] = cashRegister[50] + 1;
      if (cashRegister[25] > 0) {
        cashRegister[25] = cashRegister[25] - 1;
        return true;
      }
      return false;
    } else if (Number(money) === 100) {
      cashRegister[100] = cashRegister[100] + 1;
      if (cashRegister[50] > 0 && cashRegister[25] > [0]) {
        cashRegister[50] = cashRegister[50] - 1;
        cashRegister[25] = cashRegister[25] - 1;
        return true;
      } else if (cashRegister[25] > 2) {
        cashRegister[25] = cashRegister[25] - 3;
        return true;
      }
      return false;
    }
  }

  for (let i = 0; i < people.length; i++) {
    const isAbleToGiveChange = giveChange(people[i], cashRegister);
    if (!isAbleToGiveChange) {
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
